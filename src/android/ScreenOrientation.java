package com.phonegap.plugin.ScreenOrientation;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.pm.ActivityInfo;

import org.apache.cordova.*;

public class ScreenOrientation extends CordovaPlugin {
    // Refer to http://developer.android.com/reference/android/R.attr.html#screenOrientation

    private static final String LANDSCAPE = "landscape";
    private static final String PORTRAIT = "portrait";
    private static final String REVERSE_LANDSCAPE = "reverseLandscape";
    private static final String REVERSE_PORTRAIT = "reversePortrait";
    private static final String FULL_SENSOR = "fullSensor";

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

    	if (action.equals("set")) {
    		String orientation = args.optString(0);
    		Activity activity = this.cordova.getActivity();
    		 if (orientation.equals(LANDSCAPE)) {
    			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    		} else if (orientation.equals(PORTRAIT)) {
    			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    		} else if (orientation.equals(REVERSE_LANDSCAPE)) {
    			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
    		} else if (orientation.equals(REVERSE_PORTRAIT)) {
    			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
    		} else if (orientation.equals(FULL_SENSOR)) {
    			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    		}
    		callbackContext.success();
    		return true;
    	} else {
    		return false;
    	}
	}
}
